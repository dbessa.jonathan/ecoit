# EcoIT

EcoIT. Son objectif est
d’être une plateforme d’éducation permettant à tout instructeur expert en accessibilité et en
éco-conception web de présenter des modules de cours.

## Environnement de développement

### Pré-requis
* PHP 8.0
* Composer
* Symfony CLI
* Docker
* Docker-compose

Pré-requis vérifiable (sauf Docker & Docker-compose) avec la commande de symfony CLI :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer i
npm i
npm run build
docker-compose up -d
symfony serve -d
```

### Ajouter des données de tests

```bash
symfony console doctrine:fixtures:load
```

## License
[MIT](https://choosealicense.com/licenses/mit/)