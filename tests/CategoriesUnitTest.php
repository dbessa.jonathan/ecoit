<?php

namespace App\Tests;

use App\Entity\Categories;
use PHPUnit\Framework\TestCase;

class CategoriesUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new Categories();

        $user->setName('name')
             ->setDescription('description')
             ->setSlug('slug');

        $this->assertTrue($user->getName() === 'name');
        $this->assertTrue($user->getDescription() === 'description');
        $this->assertTrue($user->getSlug() === 'slug');
    }
    public function testIsFalse(): void
    {
        $user = new Categories();

        $user->setName('name')
             ->setDescription('description')
             ->setSlug('slug');

        $this->assertFalse($user->getName() === 'false');
        $this->assertFalse($user->getDescription() === 'false');
        $this->assertFalse($user->getSlug() === 'false');
    }
    public function testIsEmpty()
    {
        $user = new Categories();

        $this->assertEmpty($user->getName());
        $this->assertEmpty($user->getDescription());
        $this->assertEmpty($user->getSlug());
    }
}
