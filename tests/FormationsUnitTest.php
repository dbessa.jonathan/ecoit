<?php

namespace App\Tests;

use App\Entity\Formations;
use DateTime;
use PHPUnit\Framework\TestCase;

class FormationsUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $formations = new Formations();
        $dateTime = new DateTime();

        $formations->setTitle('title')
             ->setDescription('description')
             ->setContent('content')
             ->setCreationDate($dateTime)
             ->setSlug('slug');

        $this->assertTrue($formations->getTitle() === 'title');
        $this->assertTrue($formations->getDescription() === 'description');
        $this->assertTrue($formations->getContent() === 'content');
        $this->assertTrue($formations->getCreationDate() === $dateTime);
        $this->assertTrue($formations->getSlug() === 'slug');
    }
    public function testIsFalse(): void
    {
        $formations = new Formations();
        $dateTime = new DateTime();

        $formations->setTitle('title')
             ->setDescription('description')
             ->setContent('content')
             ->setCreationDate($dateTime)
             ->setSlug('slug');

        $this->assertFalse($formations->getTitle() === 'false');
        $this->assertFalse($formations->getDescription() === 'false');
        $this->assertFalse($formations->getContent() === 'false');
        $this->assertFalse($formations->getCreationDate() === 'false');
        $this->assertFalse($formations->getSlug() === 'false');
    }
    public function testIsEmpty()
    {
        $formations = new Formations();

        $this->assertEmpty($formations->getTitle());
        $this->assertEmpty($formations->getDescription());
        $this->assertEmpty($formations->getContent());
        $this->assertEmpty($formations->getCreationDate());
        $this->assertEmpty($formations->getSlug());
    }
}
