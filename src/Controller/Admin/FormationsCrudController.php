<?php

namespace App\Controller\Admin;

use App\Entity\Formations;
use App\Entity\Users;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class FormationsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Formations::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            BooleanField::new('is_published'),
            AssociationField::new('category'),
            AssociationField::new('tag'),
            ImageField::new('file')->setBasePath('uploads/formations')->onlyOnIndex(),
            TextField::new('title'),
            TextareaField::new('description'),
            TextField::new('imageFile')->setFormType(VichImageType::class),
            TextareaField::new('content')->hideOnIndex(),
            SlugField::new('slug')->setTargetFieldName('title')->hideOnIndex(),
            DateField::new('creation_date'),
            AssociationField::new('author'),
        ];
    }
}
