<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class Teacher extends AbstractController
{
    #[Route('/formateurs', name: 'app_teacher')]
    public function index(UserRepository $userRepository,
                          PaginatorInterface $paginator,
                          Request $request): Response
    {
        $data = $userRepository->getAllTeacher('ROLE_INSTRUCTEUR');
        $teachers = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('teacher/index.html.twig', [
            'teachers' => $teachers,
        ]);
    }
    #[Route("/formateurs/{id}", name: 'app_profile')]
    public function profile(User $user)
    {
        return $this->render('teacher/profile.html.twig', [
            'teacher' => $user,
        ]);
    }
}
