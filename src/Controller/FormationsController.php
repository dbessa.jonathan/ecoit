<?php

namespace App\Controller;

use App\Entity\Formations;
use App\Repository\CategoriesRepository;
use App\Repository\FormationsRepository;
use App\Repository\TagsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormationsController extends AbstractController
{
    #[Route('/formations', name: 'app_formations')]
    public function index(CategoriesRepository $categoriesRepository,
                          FormationsRepository $formationsRepository,
                          TagsRepository $tagsRepository,
                          PaginatorInterface $paginator,
                          Request $request): Response
    {
        $data = $formationsRepository->findAll();
        $formations = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('formations/index.html.twig', [
            'categories' => $categoriesRepository->findAll(),
            'tag' => $tagsRepository->findAll(),
            'formations' => $formations,
        ]);
    }
    #[Route("/formation/{slug}", name: 'app_formation')]
    public function getFormation(Formations $formation)
    {
        return $this->render('formations/getFormation.html.twig', [
            'formation' => $formation,
        ]);
    }
}
