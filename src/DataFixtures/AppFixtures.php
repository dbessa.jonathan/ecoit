<?php

namespace App\DataFixtures;

use App\Entity\Categories;
use App\Entity\Formations;
use App\Entity\Tags;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    
    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 10; $i ++) {
            $user = new User();
            $user->setEmail($faker->email())
                ->setFirstname($faker->firstName())
                ->setLastname($faker->lastName())
                ->setRoles($faker->randomElements(['ROLE_INSTRUCTEUR', 'ROLE_APPRENANT'], $count = 1, $allowDuplicates = false))
                ->setProfile($faker->url())
                ->setInstagram($faker->url())
                ->setTwitter($faker->url())
                ->setTitle($faker->word());
            $password = $this->encoder->hashPassword($user, 'password');
            $user->setPassword($password);
            $manager->persist($user);
        }
        $user = new User();
            $user->setEmail('admin@admin.com')
                ->setFirstname('Admin')
                ->setLastname('Admin')
                ->setRoles(['ROLE_ADMIN'])
                ->setProfile('DKAO')
                ->setInstagram('DKAO')
                ->setTwitter('DKAO')
                ->setTitle('Administrateur');
            $password = $this->encoder->hashPassword($user, 'admin');
            $user->setPassword($password);
            $manager->persist($user);

        for ($i=0; $i < 5; $i ++) {
            $tag = new Tags();
            $tag->setName($faker->word());
            $manager->persist($tag);
        }


        // création 10 formations
        for ($i=0; $i < 10; $i++) {
            $formation = new Formations();

            $formation->setAuthor($user)
                      ->setTitle($faker->word(3, true))
                      ->setCreationDate($faker->dateTimeBetween('-6 month', 'now'))
                      ->setDescription($faker->text(50))
                      ->setContent($faker->text(350))
                      ->setDifficulty($faker->numberBetween(0, 5))
                      ->setIsPublished($faker->boolean(1))
                      ->setFile($faker->url())
                      ->setSlug($faker->slug());
            $manager->persist($formation);
        }

        for ($k=0; $k < 5; $k++) {
            $categorie = new Categories();

            $categorie->setName($faker->word())
                      ->setDescription($faker->words(3, true))
                      ->setSlug($faker->slug());
            $manager->persist($categorie);
        }
        
        // création de 2 Formations & catégories
        for ($j=0; $j < 2; $j++) {
            $formation = new Formations();

            $formation->setAuthor($user)
                      ->setTitle($faker->word(3, true))
                      ->setCreationDate($faker->dateTimeBetween('-6 month', 'now'))
                      ->setDescription($faker->text(50))
                      ->setContent($faker->text(350))
                      ->setIsPublished($faker->boolean(1))
                      ->setTag($tag)
                      ->setFile($faker->url())
                      ->setCategory($categorie)
                      ->setSlug($faker->slug());
            $manager->persist($formation);
        }


        $manager->flush();
    }
}
